python-simpy (2.3.1+dfsg-6) unstable; urgency=medium

  * Team upload.
  * Add build-dependency on python3-setuptools (Closes: #1047295)

 -- Alexandre Detiste <tchet@debian.org>  Fri, 07 Feb 2025 23:14:12 +0100

python-simpy (2.3.1+dfsg-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + python-simpy-doc: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 22:39:10 +0100

python-simpy (2.3.1+dfsg-4) unstable; urgency=medium

  * debian/patches/doc-disable-annotate-extension.patch
    - disable extension which fails with recent Sphinx, also removed
      upstream in latest releases; Closes: #955055

 -- Sandro Tosi <morph@debian.org>  Mon, 20 Jun 2022 01:39:39 -0400

python-simpy (2.3.1+dfsg-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Sandro Tosi <morph@debian.org>  Sat, 04 Jun 2022 12:12:58 -0400

python-simpy (2.3.1+dfsg-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove trailing whitespaces
  * Convert git repository from git-dpm to gbp layout
  * d/watch: Use https protocol
  * Use debhelper-compat instead of debian/compat.

  [ Sandro Tosi ]
  * Drop python2 support; Closes: #938174
  * Update upstream homepage to point to GitLab
  * debian/watch
    - point to PyPI

 -- Sandro Tosi <morph@debian.org>  Thu, 16 Jan 2020 19:13:14 -0500

python-simpy (2.3.1+dfsg-1) unstable; urgency=low

  * Team upload

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * Use local objects.inv for intersphinx mapping (Closes: #830607)
  * Added dh-python to build depends

  [ Andreas Tille ]
  * cme fix dpkg-control
  * debhelper 9
  * remove doc/html from upstream source

 -- Andreas Tille <tille@debian.org>  Mon, 08 Aug 2016 10:48:15 +0200

python-simpy (2.3.1-1) unstable; urgency=low

  * New upstream release
    - Add a patch to fix the sphinx documentation build
    - Add a patch to fix README.txt's encoding for the python3 build
  * Add a python3-simpy binary package (SimPy now supports Python3!)
  * Update to Standards-Version 3.9.3 (no changes)
  * Update the copyright file
    - Add the new contributors
    - Update format to 1.0
  * Run the tests at build-time (adding a Build-Depend on python-pytest)

 -- Nicolas Dandrimont <nicolas.dandrimont@crans.org>  Tue, 17 Apr 2012 20:24:05 +0200

python-simpy (2.2-1) unstable; urgency=low

  * New upstream release (Closes: #644671)

 -- Nicolas Dandrimont <nicolas.dandrimont@crans.org>  Wed, 12 Oct 2011 22:25:38 +0200

python-simpy (2.1.0-1) unstable; urgency=low

  * New upstream release (Closes: #463044)
  * Take over the package under the Python Modules Team.
     - Thanks Antal A. Buss for your work on this package.

  * Add debian/watch file
  * Update Standards-Version to 3.9.2 (no changes)
  * Bump dehelper compat level to 8
  * Switch to dh_python2
  * Switch to the 3.0 (quilt) source format
  * Build the documentation from source
  * Drop old Conflicts/Replaces
  * Update the copyright file, switch to DEP5

 -- Nicolas Dandrimont <nicolas.dandrimont@crans.org>  Sat, 20 Aug 2011 15:26:35 +0200

python-simpy (1.8-1) unstable; urgency=low

  * New upstream release (Closes: #409997)
  * add Homepage fields and remove Homepage from package description.
  * Removed pycompat file and X{B|S}-Python related fields

 -- Antal A. Buss <abuss@puj.edu.co>  Sun,  9 Dec 2007 02:08:36 -0500

python-simpy (1.7.1-1) unstable; urgency=low

  * New upstream release (Closes: #359028, #340295, #376083)
  * Acknowledge NMU (Closes: #373354)
  * Rewrite rules to follow new Python policy using python-support
  * Removed python2.3-simpy and python2.3-simpy-gui.

 -- Antal A. Buss <abuss@puj.edu.co>  Wed, 16 Aug 2006 23:19:50 -0500

python-simpy (1.6-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Update package to the last python policy (Closes: #373354).

 -- Pierre Habouzit <madcoder@debian.org>  Sun,  2 Jul 2006 15:25:51 +0200

python-simpy (1.6-1) unstable; urgency=low

  * New upstream release (closes: #313350)

 -- Antal A. Buss <abuss@puj.edu.co>  Mon, 13 Jun 2005 01:21:18 -0500

python-simpy (1.5.1-3) unstable; urgency=low

  * Fix section conflict in doc package

 -- Antal A. Buss <abuss@puj.edu.co>  Sun, 27 Mar 2005 21:36:06 -0500

python-simpy (1.5.1-2) unstable; urgency=low

  * Fix build depends (Closes: #300741)

 -- Antal A. Buss <abuss@puj.edu.co>  Sun, 20 Mar 2005 23:58:33 -0500

python-simpy (1.5.1-1) unstable; urgency=low

  * New upstream release (Closes: #285003, #293680)
  * New maintainer (Closes: #206274)
  * Moved docs in separated package.

 -- Antal A. Buss <abuss@puj.edu.co>  Sat,  5 Feb 2005 03:00:48 -0500

python-simpy (1.4.2-2) unstable; urgency=low

  * QA Upload
  * Remove GUIdemo.py from python-simpy (closes: 260679)

 -- Pierre Machard <pmachard@debian.org>  Fri, 23 Jul 2004 19:09:20 +0200

python-simpy (1.4.2-1) unstable; urgency=low

  * QA Upload
  * Add python as Build-Depends (closes: #259123)
  * New upstream release (closes: #242988, #252003)

 -- Pierre Machard <pmachard@debian.org>  Mon, 19 Jul 2004 00:56:33 +0200

python-simpy (1.4-0.1) unstable; urgency=low

  * NMU.
  * New upstream release, thanks to Tony Vignaux (Closes: #230731).
  * New GUI stuff: Moved in separate package.
  * Changed build to CDBS and latest debhelper.

 -- W. Borgert <debacle@debian.org>  Wed, 04 Feb 2004 22:02:20 +0000

python-simpy (1.3-0.2) unstable; urgency=low

  * NMU.
  * Fix python interpreter in python2.3-simpy package (closes: #207316).

 -- Matthias Klose <doko@debian.org>  Fri,  5 Sep 2003 23:01:11 +0200

python-simpy (1.3-0.1) unstable; urgency=low

  * New upstream (closes: #192526, #198586).
  * Depends on Debian's default Python version 2.3.
  * Orphaned the package.

 -- W. Borgert <debacle@debian.org>  Sun, 10 Aug 2003 18:54:12 +0200

python-simpy (1.0.1-1) unstable; urgency=low

  * Initial Release.

 -- W. Borgert <debacle@debian.org>  Sun, 12 Jan 2003 12:53:57 +0000
